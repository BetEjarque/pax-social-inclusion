
# Bar chart
# It plots the inclusion of social groups in Peace Agreements over time
# Arguments:
# social_group = a string in "GCh" (Children/Your), "GDis" (Disabled), "GAge" (Elderly), 
# "GMig" (Migrants), "GRa" (Racial/Ethnic), "GRe" (Religon groups), "GInd" (Indigenous), "GRef" (Refugees), 
# "GSoc" (social class), "GOth" (other), "GeWom" (Women), "GeMe" (Men), "GeLgbti" (LGBTI),
# "GAny" (any of the previous groups)

groups.temporal_bar <- function(social_group = 'GeWom'){
    
    
    df <- pax_data %>%
        select(Dat, GCh, GDis, GAge, GMig, GRa, GRe, GInd, GRef, GSoc, GOth, GeWom, GeMe, GeLgbti) %>%
        mutate_at(vars(starts_with("G")), ~ifelse(.>0, T, F)) %>%
        rowwise() %>%
        mutate(GAny = any(GCh, GDis, GAge, GMig, GRa, GRe, GInd, GRef, GSoc, GOth, GeWom, GeMe, GeLgbti)) %>%
        ungroup() %>%
        rename("selected_group" = social_group) %>%
        mutate(categoria = case_when(
            rowSums(select(., -Dat))==0 ~ "No group is mentioned",
            selected_group ~ "Selected group is mentioned",
            TRUE ~ "Other groups are mentioned"
        )) %>%
        mutate(Year = year(Dat)) %>%
        select(Year, categoria) %>%
        group_by(Year, categoria) %>%
        summarise(n=n()) %>%
        pivot_wider(names_from=categoria, values_from=n)

    
    if (social_group != 'GAny'){
        fig <- plot_ly(df, x = ~Year, y = ~`Selected group is mentioned`, type = 'bar', name = 'Selected group is mentioned', marker = list(color = 'orange')) %>% 
            add_trace(y = ~`Other groups are mentioned`, name = 'Other groups are mentioned', marker = list(color = 'gray')) %>%
            add_trace(y = ~`No group is mentioned`, name = 'No group is mentioned', marker = list(color = 'lightgray')) %>% 
            layout(yaxis = list(title = 'Nr of Peace Agreements'), 
                   barmode = 'stack', 
                   legend = list(x = 0.1, y = 0.9, bgcolor = 'rgba(255,255,255,0.5)')
                   ) 
            
    } else {
        fig <- plot_ly(df, x = ~Year, y = ~`Selected group is mentioned`, type = 'bar', name = 'Selected group is mentioned', marker = list(color = 'orange')) %>% 
            add_trace(y = ~`No group is mentioned`, name = 'No group is mentioned', marker = list(color = 'lightgray')) %>% 
            layout(yaxis = list(title = 'Nr of Peace Agreements'), 
                   barmode = 'stack', 
                   legend = list(x = 0.1, y = 0.9, bgcolor = 'rgba(255,255,255,0.5)')
                   ) 
    }
    
    return(fig)
    
}

    
    


    
