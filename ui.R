
# Icon galleries:
# https://glyphicons.bootstrapcheatsheets.com/
# https://fontawesome.com/icons?d=gallery
# https://shiny.rstudio.com/gallery/




ui <- bootstrapPage(
  
  tags$style("
        #controls {
  /* Appearance */
  background-color: white;
  padding: 0 10px 10px 10px;
  cursor: move;
  /* Fade out while not hovering */
  opacity: 0.55;
  zoom: 0.9;
  transition: opacity 500ms 1s;
}

#controls:hover {
  /* Fade in while hovering */
  opacity: 0.75;
  transition-delay: 0;
}
               "),
  
  navbarPage("Social Inclusion in Peace Agreements",
             
             theme = shinythemes::shinytheme("flatly"), 
             
             
             tabPanel("Visualization",
                      
                      mainPanel(
                        girafeOutput("plot") %>% withSpinner(color="#4682B4")
                      ),
                      
                      absolutePanel(
                        
                        id = "map-box", class = "panel panel-default",
                        top = 80, right = 20, width = 350, fixed=FALSE,
                        draggable = FALSE, height = 150,
                        
                        h5("Inclusion of social groups around the world"),
                        leafletOutput("groups.map", height = 250) %>% withSpinner(color="#4682B4"),
                        
                        h5("Inclusion of social groups over time"),
                        plotlyOutput("groups.temporal_bar", height = 250) %>% withSpinner(color="#4682B4")
                        
                      ),
                      
                      
                      absolutePanel(
                        id = "controls", class = "panel panel-default",
                        top = 80, left = 20, width = 250, fixed=FALSE,
                        draggable = TRUE, height = "auto",
                        
                        span(tags$i(h5("Since 1990, 871 Peace Agreements have been signed (gray circles) with provisions that include specific social groups (coloured dots).")), style="color:#045a8d"),
                        
                        prettyRadioButtons(
                          "ui_sel_group",
                          "Highlight a social group:",
                          c("All" = "GAny",
                            "Elderly" = "GAge",
                            "Children/Youth" = "GCh",
                            "Disabled" = "GDis",
                            "LGBTI" = "GeLgbti",
                            "Men" = "GeMe",
                            "Women" = "GeWom",
                            "Indigenous" = "GInd",
                            "Migrants" = "GMig",
                            "Others" = "GOth",
                            "Ethnic groups" = "GRa",
                            "Religious groups" = "GRe",
                            "Refugees" = "GRef",
                            "Social class" = "GSoc"),
                          selected="GAny"),
                        
                        
                        prettyRadioButtons(
                          "ui_sel_grouping_var",
                          "Group Peace Agreeements by:",
                          c("Peace Process" = "PP",
                            "Stage" = "Stage",
                            "No grouping" = "GAny"
                          ),
                          selected = "GAny"
                        ),
                        
                        prettyCheckbox(
                          inputId = "ShowLabels",
                          label = "Show labels",
                          icon = icon("check"),
                          animation = "jelly",
                          status = "info"
                        )
                        
                      )
                      
             ),
             
             tabPanel("About", 
                      h6("Elisabet Ejarque Gonzalez"),
                      h6("Gener de 2021"),
                      h6("Visualització de Dades, PAC4"),
                      h6("Màster en Ciència de Dades, Universitat Oberta de Catalunya.")
                      
                      )
             
  )
  
  
  
)



